// ВОПРОС:
// Опишите своими словами, что такое метод обьекта
// ОТВЕТ:
// Метод объекта - это его свойство, значение которого - функция.

// ЗАДАНИЕ:
// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//    - Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//    - При вызове функция должна спросить у вызывающего имя и фамилию.
//    - Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//    - Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
//      соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
//
//    - Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
//    - Вывести в консоль результат выполнения функции.


function createNewUser() {
    this.firstName = prompt('Enter you first name: ', '');
    while (this.firstName === '' || isNaN(this.firstName) !== true) {
        this.firstName = prompt('Enter you first name AGAIN: ', '');
    }

    this.lastName = prompt('Enter you last name', '');
    while (this.lastName === '' || isNaN(this.lastName) !== true) {
        this.lastName = prompt('Enter you last name AGAIN: ', '');
    }

    this.getLogin = function () {
        let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    }
}

let newUser = new createNewUser();
console.log(newUser.getLogin());